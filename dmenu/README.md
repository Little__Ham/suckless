# dmenu (dynamic menu)
See [suckless.org](https://tools.suckless.org/dmenu) for more information.

## List of patches applied
- [fuzzyhighlight](https://tools.suckless.org/dmenu/patches/fuzzyhighlight)
- [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch)
- [morecolor](https://tools.suckless.org/dmenu/patches/morecolor)
- [password](https://tools.suckless.org/dmenu/patches/password)
