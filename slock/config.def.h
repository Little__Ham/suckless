/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#10100e",     /* after initialization */
	[INPUT] =  "#9a4eae",   /* during input */
	[FAILED] = "#c40233",   /* wrong password */
	[CAPS] =   "#0087bd",         /* CapsLock on */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
