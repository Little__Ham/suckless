# slock (simple locker)
See [suckless.org](https://tools.suckless.org/slock) for more information.

## List of patches applied
- [capscolor](https://tools.suckless.org/slock/patches/capscolor)
