# tabbed
See [suckless.org](https://tools.suckless.org/tabbed) for more information.

## List of patches applied
- [hidetabs](https://tools.suckless.org/tabbed/patches/hidetabs)
- [keyrelease](https://tools.suckless.org/tabbed/patches/keyrelease)
