# dwm (dynamic window manager)
See [suckless.org](https://dwm.suckless.org) for more information.

## List of patches applied
- [attachaside](https://dwm.suckless.org/patches/attachaside)
- [azerty](https://dwm.suckless.org/patches/azerty)
- [default tag apps](https://dwm.suckless.org/patches/default_tag_apps)
- [dmenumatchtop](https://dwm.suckless.org/patches/dmenumatchtop)
- [fakefullscreen](https://dwm.suckless.org/patches/fakefullscreen)
- [horizgrid](https://dwm.suckless.org/patches/horizgrid)
- [layoutscroll](https://dwm.suckless.org/patches/layoutscroll)
- [movestack](https://dwm.suckless.org/patches/movestack)
- [pertag without bar](https://dwm.suckless.org/patches/pertag)
- [singularborders](https://dwm.suckless.org/patches/singularborders)
- [taggrid](https://dwm.suckless.org/patches/taggrid)
