# st (simple terminal)
See [suckless.org](https://st.suckless.org) for more information.

## List of patches applied
- [anysize](https://st.suckless.org/patches/anysize)
- [csi 22 23](https://st.suckless.org/patches/csi_22_23/)
- [font2](https://st.suckless.org/patches/font2)
- [scrollback ringbuffer](https://st.suckless.org/patches/scrollback)
