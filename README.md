# Personal builds of suckless tools
This repository houses my patched builds of [suckless](https://suckless.org) tools.\
They all use the [Blood Moon](https://github.com/dguo/blood-moon) color scheme from Danny Guo.

## List of patches applied
### dmenu
- [fuzzyhighlight](https://tools.suckless.org/dmenu/patches/fuzzyhighlight)
- [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch)
- [morecolor](https://tools.suckless.org/dmenu/patches/morecolor)
- [password](https://tools.suckless.org/dmenu/patches/password)

### dwm
- [attachaside](https://dwm.suckless.org/patches/attachaside)
- [azerty](https://dwm.suckless.org/patches/azerty)
- [default tag apps](https://dwm.suckless.org/patches/default_tag_apps)
- [dmenumatchtop](https://dwm.suckless.org/patches/dmenumatchtop)
- [fakefullscreen](https://dwm.suckless.org/patches/fakefullscreen)
- [horizgrid](https://dwm.suckless.org/patches/horizgrid)
- [layoutscroll](https://dwm.suckless.org/patches/layoutscroll)
- [movestack](https://dwm.suckless.org/patches/movestack)
- [pertag without bar](https://dwm.suckless.org/patches/pertag)
- [singularborders](https://dwm.suckless.org/patches/singularborders)
- [taggrid](https://dwm.suckless.org/patches/taggrid)

### slock
- [capscolor](https://tools.suckless.org/slock/patches/capscolor)

### st
- [anysize](https://st.suckless.org/patches/anysize)
- [csi 22 23](https://st.suckless.org/patches/csi_22_23)
- [font2](https://st.suckless.org/patches/font2)
- [scrollback ringbuffer](https://st.suckless.org/patches/scrollback)

### tabbed
- [hidetabs](https://tools.suckless.org/tabbed/patches/hidetabs)
- [keyrelease](https://tools.suckless.org/tabbed/patches/keyrelease)
